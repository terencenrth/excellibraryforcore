﻿using System;
using System.Collections.Generic;
using CoreExcel;

namespace ExcelLibraryForCore
{
    class Program
    {
     
        
        static void Main(string[] args)
        {
            List<UserDetails> persons = new List<UserDetails>()
            {
               
                new UserDetails() {fins=new List<Fin>(){ new Fin() { Yeah="What" },new Fin { Yeah="Now" } },ID="1001", Name="ABCD", City ="City1", Country="USA", Country1="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country11="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country2="Lorem Ipsum is simply dummy text of the printing and typesetting ",Country21="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country3="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country31="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country4="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country41="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country5="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country51="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country6="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country64="Lorem Ipsum is simply dummy text of the printing and typesetting "},
                new UserDetails() {ID="1002", Name="PQRS", City ="City2", Country="INDIA", Country1="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country11="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country2="Lorem Ipsum is simply dummy text of the printing and typesetting ",Country21="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country3="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country31="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country4="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country41="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country5="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country51="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country6="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country64="Lorem Ipsum is simply dummy text of the printing and typesetting "},
                new UserDetails() {ID="1003", Name="XYZZ", City ="City3", Country="CHINA", Country1="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country11="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country2="Lorem Ipsum is simply dummy text of the printing and typesetting ",Country21="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country3="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country31="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country4="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country41="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country5="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country51="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country6="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country64="Lorem Ipsum is simply dummy text of the printing and typesetting "},
                new UserDetails() {ID="1004", Name="LMNO", City ="City4", Country="UK", Country1="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country11="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country2="Lorem Ipsum is simply dummy text of the printing and typesetting ",Country21="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country3="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country31="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country4="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country41="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country5="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country51="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country6="Lorem Ipsum is simply dummy text of the printing and typesetting ", Country64="Lorem Ipsum is simply dummy text of the printing and typesetting "},
           };

          
            var table = CoreExcelBuilder.GetTableSimple<UserDetails>(persons);
            CoreExcelBuilder.CreateFileExcel(table, "F:\\Excel\\tes.xlsx",null);
            Console.ReadKey();


        }
        public class Fin
        {
            public string Yeah { get; set; }
            public string date { get; set; } = DateTime.Now.ToString();
        }
        public class UserDetails
        {
            public List<Fin> fins{get;set;}
            public string ID { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string Country1 { get; set; }
            public string Country2 { get; set; }
            public string Country3 { get; set; }
            public string Country4 { get; set; }
            public string Country5 { get; set; }
            public string Country6 { get; set; }
            public string Country11 { get; set; }
            public string Country21 { get; set; }
            public string Country31 { get; set; }
            public string Country41 { get; set; }
            public string Country51 { get; set; }
            public string Country64 { get; set; }
        }
     
    }
}
