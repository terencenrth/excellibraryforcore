﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CoreExcel
{
    public static class CoreExcelBuilder
    {
        public static Stylesheet CreateStylesheet(Stylesheet stylesheet)
        {
            if (stylesheet != null)
            {
                return stylesheet;
            }
            Stylesheet stylesheet1 = new Stylesheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } };
            stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            Fonts fonts1 = new Fonts() { Count = (UInt32Value)1U, KnownFonts = true };
            //Normal Font
            Font font1 = new Font();
            FontSize fontSize1 = new FontSize() { Val = 11D };
            Color color1 = new Color() { Theme = (UInt32Value)1U };
            FontName fontName1 = new FontName() { Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme1 = new FontScheme() { Val = FontSchemeValues.Minor };

            font1.Append(fontSize1);
            font1.Append(color1);
            font1.Append(fontName1);
            font1.Append(fontFamilyNumbering1);
            font1.Append(fontScheme1);
            fonts1.Append(font1);

            //Bold Font
            Font bFont = new Font();
            FontSize bfontSize = new FontSize() { Val = 11D };
            Color bcolor = new Color() { Theme = (UInt32Value)1U };
            FontName bfontName = new FontName() { Val = "Calibri" };
            FontFamilyNumbering bfontFamilyNumbering = new FontFamilyNumbering() { Val = 2 };
            FontScheme bfontScheme = new FontScheme() { Val = FontSchemeValues.Minor };
            Bold bFontBold = new Bold();

            bFont.Append(bfontSize);
            bFont.Append(bcolor);
            bFont.Append(bfontName);
            bFont.Append(bfontFamilyNumbering);
            bFont.Append(bfontScheme);
            bFont.Append(bFontBold);

            fonts1.Append(bFont);

            Fills fills1 = new Fills() { Count = (UInt32Value)5U };

            // FillId = 0
            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.None };
            fill1.Append(patternFill1);

            // FillId = 1
            Fill fill2 = new Fill();
            PatternFill patternFill2 = new PatternFill()
            { PatternType = PatternValues.Gray125 };
            fill2.Append(patternFill2);

            // FillId = 2,RED             
            Fill fill3 = new Fill();
            PatternFill patternFill3 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor() { Rgb = "FFB6C1" };
            BackgroundColor backgroundColor1 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill3.Append(foregroundColor1);
            patternFill3.Append(backgroundColor1);
            fill3.Append(patternFill3);

            // FillId = 3,GREEN             
            Fill fill4 = new Fill();
            PatternFill patternFill4 = new PatternFill()
            { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor2 = new ForegroundColor()
            { Rgb = "90EE90" };
            BackgroundColor backgroundColor2 = new BackgroundColor()
            { Indexed = (UInt32Value)64U };
            patternFill4.Append(foregroundColor2);
            patternFill4.Append(backgroundColor2);
            fill4.Append(patternFill4);

            // FillId = 4,YELLO             
            Fill fill5 = new Fill();
            PatternFill patternFill5 = new PatternFill()
            { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor3 = new ForegroundColor()
            { Rgb = "FFFF00" };
            BackgroundColor backgroundColor3 = new BackgroundColor()
            { Indexed = (UInt32Value)64U };
            patternFill5.Append(foregroundColor3);
            patternFill5.Append(backgroundColor3);
            fill5.Append(patternFill5);

            // FillId = 5,RED and BOLD Text             
            Fill fill6 = new Fill();
            PatternFill patternFill6 = new PatternFill()
            {
                PatternType = PatternValues.Solid
            };
            ForegroundColor foregroundColor4 = new ForegroundColor()
            {
                Rgb = "FFB6C1"
            };
            BackgroundColor backgroundColor4 = new BackgroundColor()
            {
                Indexed = (UInt32Value)64U
            };
            Bold bold1 = new Bold();
            patternFill6.Append(foregroundColor4);
            patternFill6.Append(backgroundColor4);
            fill6.Append(patternFill6);

            fills1.Append(fill1);
            fills1.Append(fill2);
            fills1.Append(fill3);
            fills1.Append(fill4);
            fills1.Append(fill5);
            fills1.Append(fill6);

            Borders borders1 = new Borders() { Count = (UInt32Value)1U };

            Border border1 = new Border();
            LeftBorder leftBorder1 = new LeftBorder();
            RightBorder rightBorder1 = new RightBorder();
            TopBorder topBorder1 = new TopBorder();
            BottomBorder bottomBorder1 = new BottomBorder();
            DiagonalBorder diagonalBorder1 = new DiagonalBorder();

            border1.Append(leftBorder1);
            border1.Append(rightBorder1);
            border1.Append(topBorder1);
            border1.Append(bottomBorder1);
            border1.Append(diagonalBorder1);

            borders1.Append(border1);

            CellStyleFormats cellStyleFormats1 = new CellStyleFormats()
            { Count = (UInt32Value)1U };
            CellFormat cellFormat1 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)0U,
                FillId = (UInt32Value)0U,
                BorderId = (UInt32Value)0U
            };

            cellStyleFormats1.Append(cellFormat1);

            CellFormats cellFormats1 = new CellFormats()
            {
                Count = (UInt32Value)4U
            };
            CellFormat cellFormat2 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)0U,
                FillId = (UInt32Value)0U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U
            };
            CellFormat cellFormat3 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)0U,
                FillId = (UInt32Value)2U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };
            CellFormat cellFormat4 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)0U,
                FillId = (UInt32Value)3U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };
            CellFormat cellFormat5 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)0U,
                FillId = (UInt32Value)4U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };
            CellFormat cellFormat6 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)1U,
                FillId = (UInt32Value)2U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };
            CellFormat cellFormat7 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)1U,
                FillId = (UInt32Value)3U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };
            CellFormat cellFormat8 = new CellFormat()
            {
                NumberFormatId = (UInt32Value)0U,
                FontId = (UInt32Value)1U,
                FillId = (UInt32Value)4U,
                BorderId = (UInt32Value)0U,
                FormatId = (UInt32Value)0U,
                ApplyFill = true
            };

            cellFormats1.Append(cellFormat2);
            cellFormats1.Append(cellFormat3);
            cellFormats1.Append(cellFormat4);
            cellFormats1.Append(cellFormat5);
            cellFormats1.Append(cellFormat6);
            cellFormats1.Append(cellFormat7);
            cellFormats1.Append(cellFormat8);

            CellStyles cellStyles1 = new CellStyles()
            {
                Count = (UInt32Value)1U
            };
            CellStyle cellStyle1 = new CellStyle()
            {
                Name = "Normal",
                FormatId = (UInt32Value)0U,
                BuiltinId = (UInt32Value)0U
            };

            cellStyles1.Append(cellStyle1);
            DifferentialFormats differentialFormats1 = new DifferentialFormats() { Count = (UInt32Value)0U };
            TableStyles tableStyles1 = new TableStyles()
            {
                Count = (UInt32Value)0U,
                DefaultTableStyle = "TableStyleMedium2",
                DefaultPivotStyle = "PivotStyleMedium9"
            };

            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellStyleFormats1);
            stylesheet1.Append(cellFormats1);
            stylesheet1.Append(cellStyles1);
            stylesheet1.Append(differentialFormats1);
            stylesheet1.Append(tableStyles1);

            return stylesheet1;
        }

        public static string ConvertToBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes, 0, bytes.Length);
        }

        public static byte[] CreateStreamExcel(DataTable table, MemoryStream stream, Stylesheet stylesheett)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                WorkbookStylesPart stylePart = workbookPart.AddNewPart
                       <WorkbookStylesPart>();
                stylePart.Stylesheet = CreateStylesheet(null);
                stylePart.Stylesheet.Save();
                workbookPart.Workbook = new Workbook();


                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                Columns columns1 = new Columns();

                worksheetPart.Worksheet = new Worksheet(columns1, sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                sheets.Append(sheet);



                for (uint i = 1; i < (uint)table.Columns.Count + 1; i++)
                {
                    Column column1 = new Column()
                    {
                        Min = (UInt32Value)i,
                        Max = (UInt32Value)i,
                        Width = 40D,
                        BestFit = true,
                        CustomWidth = true
                    };
                    columns1.Append(column1);
                }

                Row headerRow = new Row();

                List<string> columns = new List<string>();
                foreach (DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    Cell cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.StyleIndex = (UInt32Value)6;
                    cell.CellValue = new CellValue(Regex.Replace(column.ColumnName, "((?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z]))", " $1").Trim());

                    headerRow.AppendChild(cell);
                }

                sheetData.AppendChild(headerRow);

                foreach (DataRow dsrow in table.Rows)
                {
                    Row newRow = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(dsrow[col].ToString());
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }


                workbookPart.Workbook.Save();

            }
            byte[] bytes = stream.ToArray();
            return bytes;
        }
        public static void CreateFileExcel(DataTable table,string FilePath,Stylesheet stylesheet)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(FilePath, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                WorkbookStylesPart stylePart = workbookPart.AddNewPart
                       <WorkbookStylesPart>();
                stylePart.Stylesheet = CreateStylesheet(null);
                stylePart.Stylesheet.Save();
                workbookPart.Workbook = new Workbook();


                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                Columns columns1 = new Columns();

                worksheetPart.Worksheet = new Worksheet(columns1, sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                sheets.Append(sheet);



                for (uint i = 1; i < (uint)table.Columns.Count+1; i++)
                {
                    Column column1 = new Column()
                    {
                        Min = (UInt32Value)i,
                        Max = (UInt32Value)i,
                        Width = 40D,
                        BestFit = true,
                        CustomWidth = true
                    };
                    columns1.Append(column1);
                }

                Row headerRow = new Row();

                List<string> columns = new List<string>();
                foreach (DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    Cell cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.StyleIndex = (UInt32Value)6;
                    cell.CellValue = new CellValue(Regex.Replace(column.ColumnName, "((?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z]))", " $1").Trim());

                    headerRow.AppendChild(cell);
                }

                sheetData.AppendChild(headerRow);

                foreach (DataRow dsrow in table.Rows)
                {
                    Row newRow = new Row();
                    foreach (string col in columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(dsrow[col].ToString());
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }


                workbookPart.Workbook.Save();

            }
        }

       

        public static List<string> GetClassPropertyNames(Type myClass)
        {
            PropertyInfo[] propertyInfos;
            propertyInfos = myClass.GetProperties();

            List<string> propertyTypeNames = new List<string>();

            // write property names
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                propertyTypeNames.Add(propertyInfo.Name);
            }

            return propertyTypeNames;

        }
        public static DataTable GetTableSimple<T>(IList<T> list)
        {

            List<JObject> test = new List<JObject>();

            foreach (var item in list)
            {
                var obj = new JObject();
                var fields = GetClassPropertyNames(item.GetType());
                foreach (var i in fields)
                {
                    var field = i;
                    var value = item.GetType().GetProperty(field).GetValue(item) == null ? "" : item.GetType().GetProperty(field).GetValue(item).ToString();
                    if (value.Contains("System.Collections.Generic.List"))
                    {
                        var count = 1;
                        foreach (var x in (IEnumerable<object>)item.GetType().GetProperty(field).GetValue(item))
                        {
                            var fields2 = GetClassPropertyNames(x.GetType());
                           
                            foreach (var n in fields2)
                            {
                                var value2 = x.GetType().GetProperty(n).GetValue(x) == null ? "" : x.GetType().GetProperty(n).GetValue(x).ToString();
                                obj.Add(n +count, value2);
                                
                            }
                            count++;
                        }
                    }
                    else
                    {
                        if (value != "")
                        {
                            obj.Add(field, value);
                        }
                      
                    }
                   

                }
                test.Add(obj);

            }
            return (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(test), typeof(DataTable));
        }
    }
    public class ExcelViewModel
    {
        public string FileName { get; set; }
        public string File { get; set; }
    }
}
